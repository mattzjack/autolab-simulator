from thing import Thing
import random

class Test(Thing):
    def __init__(self, topY):
        self.height = 200
        self.topY = topY
        self.x = 100
        self.color = 'yellow'

    def move(self, speed):
        self.topY -= speed
        self.x += 4

    def outOfScreen(self):
        return self.topY + self.height < 0

    def draw(self, canvas, width, height):
        canvas.create_rectangle(self.x - 100, self.topY, self.x + 100, self.topY + 200, fill=self.color)

    def keyPressed(self, key):
        self.color = random.choice(['red', 'green', 'blue', 'yellow'])

