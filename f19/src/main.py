from cmu_112_graphics import *
from tkinter import *
from threading import Thread
import time, pyaudio, wave

from test import Test

class AutolabSimulator(ModalApp):
    def appStarted(app):
        # set fps
        app.framerate = 60 # Hz
        app.timerDelay = 1000 // app.framerate # 1000 ms
        # set modes
        app.mainMode = MainMode()
        app.splashMode = SplashMode()
        app.setActiveMode(app.splashMode)

class MainMode(Mode):
    def appStarted(mode):
        # display real framerate
        mode.framecount = 0
        mode.time = time.time()
        mode.realFramerate = 0
        # things
        mode.things = [Test]
        mode.showings = []
        mode.speed = 4

    def keyPressed(mode, event):
        if event.key == 'Space' and len(mode.things) > 0:
            mode.showings.append(mode.things.pop(0)(mode.height))
        else:
            for thing in mode.showings:
                thing.keyPressed(event.key)

    def timerFired(mode):
        # display real framerate
        mode.framecount += 1
        t = time.time()
        if t - mode.time >= 1:
            mode.time = t
            mode.realFramerate = mode.framecount
            mode.framecount = 0
        # move and remove things
        for thing in mode.showings:
            thing.move(mode.speed)
        if len(mode.showings) > 0 and mode.showings[0].outOfScreen():
            mode.showings.pop(0)

    def redrawAll(mode, canvas):
        # bg
        canvas.create_rectangle(0, 0, mode.width, mode.height, fill='gray10')
        # display real framerate
        canvas.create_text(mode.width, 0, text=mode.realFramerate, anchor=NE, fill='white')
        # draw things
        for thing in mode.showings:
            thing.draw(canvas, mode.width, mode.height)

class SplashMode(Mode):
    def mousePressed(mode, event):
        Thread(target=play, args=('./StillAlive.wav',)).start()
        mode.app.setActiveMode(mode.app.mainMode)

# thanks 112 audio people
def play(file):
    CHUNK = 1024

    wf = wave.open(file, 'rb')

    p = pyaudio.PyAudio()

    stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                    channels=wf.getnchannels(),
                    rate=wf.getframerate(),
                    output=True)

    data = wf.readframes(CHUNK)

    while len(data) > 0:
        stream.write(data)
        data = wf.readframes(CHUNK)

    stream.stop_stream()
    stream.close()

    p.terminate()

AutolabSimulator(width=1000, height=700)

