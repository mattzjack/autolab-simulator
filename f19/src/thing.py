class Thing(object):
    def __init__(self, topY):
        self.height = 200
        self.topY = topY

    def move(self, speed):
        self.topY -= speed

    def outOfScreen(self):
        return self.topY + self.height < 0

    def draw(self, canvas, width, height):
        canvas.create_rectangle(width / 2 - 100, self.topY, width / 2 + 100, self.topY + 200, fill='yellow')

    def keyPressed(self, key):
        pass

