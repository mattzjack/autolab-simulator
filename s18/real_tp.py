# more: marisal colab10 ascii
#       imported tkinter above ignore rest

# using 112 tkinter framework

from tkinter import *
import pyaudio, wave, threading, time, random

# START modified from 112 website #
def rgb(red, green, blue):
    return "#%02x%02x%02x" % (red, green, blue)
# END modified from 112 website #

# START online formula #
def collide_rect(x0, y0, w, h, x, y):
    return x0 <= x <= x0 + w and y0 <= y <= y0 + h
# END online formula #

def collide_oval(cx, cy, a, b, x, y):
    return ((x - cx) ** 2) / (a ** 2) + ((y - cy) ** 2) / (b ** 2) <= 1

# START pyaudio docs #
def play(file):
    CHUNK = 1024
    wf = wave.open(file, 'rb')
    p = pyaudio.PyAudio()
    stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                    channels=wf.getnchannels(),
                    rate=wf.getframerate(),
                    output=True)
    data = wf.readframes(CHUNK)
    while data != b'':
        stream.write(data)
        data = wf.readframes(CHUNK)
    stream.stop_stream()
    stream.close()
    p.terminate()
# END pyaudio docs #

class AutoSim(object):
    SPLASH = 'start'
    SCROLL = 'scroll'

    def __init__(self, width=800, height=600):
        self.width = width
        self.height = height

        self.root = Tk()
        self.canvas = Canvas(self.root, width=self.width, height=self.height)
        self.canvas.pack()

        self.root.bind('<Button-1>', self.mouse_pressed)
        self.root.bind('<Motion>', self.mouse_motion)
        self.root.bind('<Key>', self.key_pressed)

        self.timer_delay = 16
        self.prev_time = time.time()

        self.screen = AutoSim.SPLASH
        self.transition_to_scroll = False
        self.splash_transition_fill = [255, 255, 255]
        self.transition_speed = 5

        self.btn_major = min(self.width, self.height) * 3 / 8
        self.btn_minor = self.btn_major * 3 / 4
        self.btn_height = self.btn_minor / 2
        self.btn_top_fill = [250, 165, 49]
        self.btn_shade_fill = [x * 7 // 8 for x in self.btn_top_fill]

        self.btn_top_fill_init = self.btn_top_fill.copy()
        self.btn_shade_fill_init = self.btn_shade_fill.copy()
        self.btn_top_fill_brightest = [min(x * 4 // 3, 255) for x in self.btn_top_fill_init]
        self.btn_shade_fill_brightest = [min(x * 4 // 3, 255) for x in self.btn_shade_fill_init]
        self.btn_brighter = False

        self.started_music = False
        self.music_file = 'Still Alive.wav'
        self.music_dur = 2 * 60 + 56

        self.text_file = 'main.txt'
        with open(self.text_file) as f:
            self.text = f.read().strip().split('\n\n\n')
            f.close()
        self.text_top = [self.height] * len(self.text)
        self.newest_moving_text = 0
        # self.text_num_lines = len(self.text.split())
        self.text_font_size = 16
        # self.approx_total_scroll_len = self.text_font_size * self.text_num_lines * 7 / 8
        # self.text_speed = self.approx_total_scroll_len / (self.music_dur * (1000 / self.timer_delay))
        self.text_speed = 1

        self.teddy_img = PhotoImage(file='imgs/teddy.gif')
        self.teddy_y = self.height
        self.teddy_going = False

        self.frogger_img = PhotoImage(file='imgs/long_truck.gif')
        self.truck_img = PhotoImage(file='imgs/really_long_truck.gif')
        self.frogger_y = self.height
        self.truck_y = self.height
        self.truck_going = False

        self.tetris_top = self.height
        self.tetris_margin = 20
        self.tetris_num_rows = 15
        self.tetris_num_cols = 10
        self.tetris_cell_size = 20
        self.tetris_width = (2 * self.tetris_margin +
                             self.tetris_cell_size * self.tetris_num_cols)
        self.tetris_height = (2 * self.tetris_margin +
                              self.tetris_cell_size * self.tetris_num_rows)
        self.tetris_board = [[None] * self.tetris_num_cols for _ in range(self.tetris_num_rows)]
        self.tetris_piece_r = 0
        self.tetris_piece_c = 3
        self.tetris_going = False
        self.tetris_timer = 0
        self.tetris_piece = [[1, 1, 1, 1]]
        self.tetris_other_piece = [[1],
                                   [1],
                                   [1],
                                   [1]]

        self.clicker_top = self.height
        self.clicker_going = False
        self.clicker_icon = PhotoImage(file='imgs/112.gif')
        self.clicker_width = 400
        self.clicker_height = 400
        self.clicker_x = random.randrange(self.clicker_width - 50)
        self.clicker_y = random.randrange(self.clicker_height - 50)
        self.clicker_right = 1
        self.clicker_down = 1
        self.clicker_speed = 2

        self.sudoku_going = False
        self.sudoku_img = PhotoImage(file='imgs/sudoku.gif')
        self.sudoku_top = self.height

    def key_pressed(self, event):
        if self.screen == AutoSim.SCROLL:
            self.scroll_key_pressed(event)

    def scroll_key_pressed(self, event):
        if self.tetris_going:
            if event.keysym == 'Left':
                self.tetris_piece_c -= 1
            elif event.keysym == 'Right':
                self.tetris_piece_c += 1
            elif event.keysym == 'Up':
                self.tetris_piece, self.tetris_other_piece = self.tetris_other_piece, self.tetris_piece
        if event.keysym == 'space':
            if self.newest_moving_text == 21 and not self.truck_going:
                self.truck_going = True
            elif self.newest_moving_text == 13 and not self.sudoku_going:
                self.sudoku_going = True
            elif self.newest_moving_text == 14 and not self.tetris_going:
                self.tetris_going = True
            elif self.newest_moving_text == 15 and not self.clicker_going:
                self.clicker_going = True
            elif self.newest_moving_text == 20 and not self.teddy_going:
                self.teddy_going = True
            else:
                self.newest_moving_text += 1

    def mouse_pressed(self, event):
        if self.screen == AutoSim.SPLASH:
            self.splash_mouse_pressed(event)

    def splash_mouse_pressed(self, event):
        if collide_oval(self.width / 2, self.height / 2 - self.btn_height,
                        self.btn_major, self.btn_minor, event.x, event.y):
            self.transition_to_scroll = True

    def mouse_motion(self, event):
        if self.screen == AutoSim.SPLASH:
            self.splash_mouse_motion(event)

    def splash_mouse_motion(self, event):
        self.btn_brighter = (collide_oval(self.width / 2,
                                          self.height / 2 - self.btn_height,
                                          self.btn_major, self.btn_minor,
                                          event.x, event.y) or
                             collide_oval(self.width / 2, self.height / 2,
                                          self.btn_major, self.btn_minor,
                                          event.x, event.y) or
                             collide_rect(self.width / 2 - self.btn_major,
                                          self.height / 2 - self.btn_height,
                                          self.btn_major * 2, self.btn_height,
                                          event.x, event.y))

    def timer_fired(self):
        new_time = time.time()
        # print(new_time - self.prev_time)
        self.prev_time = new_time
        if self.screen == AutoSim.SPLASH:
            self.splash_timer_fired()
        elif self.screen == AutoSim.SCROLL:
            self.scroll_timer_fired()
        self.redraw_all()
        self.canvas.after(self.timer_delay, self.timer_fired)

    def scroll_timer_fired(self):
        if not self.started_music:
            threading.Thread(target=play, args=(self.music_file,)).start()
            self.started_music = True
        for i in range(min(self.newest_moving_text, len(self.text_top))):
            self.text_top[i] -= self.text_speed
        if self.truck_going:
            self.truck_y -= self.text_speed
            self.frogger_y -= self.text_speed
        if self.teddy_going:
            self.teddy_y -= self.text_speed
        if self.tetris_going:
            self.tetris_top -= self.text_speed
            self.tetris_timer += 1
            if self.tetris_timer % 50 == 0:
                self.tetris_piece_r += 1
        if self.clicker_going:
            self.clicker_top -= self.text_speed
            self.clicker_x += self.clicker_right * self.clicker_speed
            self.clicker_y += self.clicker_down * self.clicker_speed
            if self.clicker_x + 50 >= self.clicker_width:
                self.clicker_right = -1
            elif self.clicker_x <= 0:
                self.clicker_right = 1
            if self.clicker_y + 50 >= self.clicker_height:
                self.clicker_down = -1
            elif self.clicker_y <= 0:
                self.clicker_down = 1
        if self.sudoku_going:
            self.sudoku_top -= self.text_speed

    def splash_timer_fired(self):
        if self.transition_to_scroll:
            for i in range(len(self.splash_transition_fill)):
                self.splash_transition_fill[i] = max(0, self.splash_transition_fill[i] - self.transition_speed)
                self.btn_top_fill[i] = max(0, self.btn_top_fill[i] - self.transition_speed)
                self.btn_shade_fill[i] = max(0, self.btn_shade_fill[i] - self.transition_speed)
            if self.splash_transition_fill == self.btn_top_fill == self.btn_shade_fill == [0, 0, 0]:
                self.transition_to_scroll = False
                self.screen = AutoSim.SCROLL
        else:
            if self.btn_brighter:
                for i in range(len(self.btn_top_fill)):
                    if self.btn_top_fill[i] < self.btn_top_fill_brightest[i]:
                        self.btn_top_fill[i] = min(self.btn_top_fill_brightest[i],
                                                   self.btn_top_fill[i] + max(1, (self.btn_top_fill_brightest[i] - self.btn_top_fill_init[i]) // 10))
                    if self.btn_shade_fill[i] < self.btn_shade_fill_brightest[i]:
                        self.btn_shade_fill[i] = min(self.btn_shade_fill_brightest[i],
                                                     self.btn_shade_fill[i] + max(1, (self.btn_shade_fill_brightest[i] - self.btn_shade_fill_init[i]) // 10))
            else:
                for i in range(len(self.btn_top_fill)):
                    if self.btn_top_fill[i] > self.btn_top_fill_init[i]:
                        self.btn_top_fill[i] = max(self.btn_top_fill_init[i],
                                                   self.btn_top_fill[i] - max(1, (self.btn_top_fill_brightest[i] - self.btn_top_fill_init[i]) // 10))
                    if self.btn_shade_fill[i] > self.btn_shade_fill_init[i]:
                        self.btn_shade_fill[i] = max(self.btn_shade_fill_init[i],
                                                     self.btn_shade_fill[i] - max(1, (self.btn_shade_fill_brightest[i] - self.btn_shade_fill_init[i]) // 10))

    def redraw_all(self):
        self.canvas.delete(ALL)
        if self.screen == AutoSim.SPLASH:
            self.splash_redraw_all()
        elif self.screen == AutoSim.SCROLL:
            self.scroll_redraw_all()

    def scroll_redraw_all(self):
        self.scroll_draw_bg()
        self.scroll_draw_text()
        self.scroll_draw_tetris()
        # frogger_y = self.text_top[0] + self.frogger_yrel
        # truck_y = self.text_top[0] + self.truck_yrel
        if -1000 < self.frogger_y < self.height:
            self.scroll_draw_frogger(self.frogger_y)
        if -1000 < self.truck_y < self.height:
            self.scroll_draw_truck(self.truck_y)
        if -1000 < self.teddy_y < self.height:
            self.draw_teddy()
        if -1000 < self.clicker_top < self.height:
            self.draw_clicker()
        if -1000 < self.sudoku_top < self.height:
            self.draw_sudoku()

    def draw_sudoku(self):
        self.canvas.create_image(20, self.sudoku_top, anchor=NW, image=self.sudoku_img)

    def scroll_draw_truck(self, y):
        self.canvas.create_image(250, y, anchor=NW, image=self.truck_img)

    def scroll_draw_frogger(self, y):
        self.canvas.create_image(20, y, anchor=NW, image=self.frogger_img)

    def scroll_draw_tetris(self):
        if (self.tetris_top < self.height and
            self.tetris_top + self.tetris_height > 0):
            self.draw_tetris_bg()
            self.draw_tetris_grid()
            self.draw_tetris_piece()

    def draw_clicker(self):
        self.canvas.create_rectangle(20, self.clicker_top, 20 + self.clicker_width, self.clicker_top + self.clicker_height, fill='white', outline='')
        self.draw_clicker_title()
        self.draw_clicker_text()
        self.draw_clicker_icon()

    def draw_clicker_title(self):
        self.canvas.create_text(20 + self.clicker_width / 2, self.clicker_top + self.clicker_height * .25, text='The 112\nClicker Game!', font=('Arial', 50, 'bold'))

    def draw_clicker_text(self):
        self.canvas.create_text(20 + self.clicker_width / 2, self.clicker_top + self.clicker_height * .75, text="Press 'p' to play", font=('Arial', 20, 'bold'))

    def draw_clicker_icon(self):
        self.canvas.create_image(20 + self.clicker_x, self.clicker_top + self.clicker_y, anchor=NW, image=self.clicker_icon)

    def draw_tetris_bg(self):
        self.canvas.create_rectangle(20, self.tetris_top,
                                     20 + self.tetris_width,
                                     self.tetris_top + self.tetris_height,
                                     fill='orange')

    def draw_tetris_grid(self):
        for r in range(self.tetris_num_rows):
            for c in range(self.tetris_num_cols):
                if self.tetris_board[r][c] == None:
                    fill = 'blue'
                else:
                    fill = self.tetris_board[r][c]
                self.canvas.create_rectangle(20 + self.tetris_margin + self.tetris_cell_size * c,
                                             self.tetris_top + self.tetris_margin + self.tetris_cell_size * r,
                                             20 + self.tetris_margin + self.tetris_cell_size * (c + 1),
                                             self.tetris_top + self.tetris_margin + self.tetris_cell_size * (r + 1),
                                             fill=fill, width=3)

    def draw_tetris_piece(self):
        for r in range(len(self.tetris_piece)):
            for c in range(len(self.tetris_piece[r])):
                self.canvas.create_rectangle(20 + self.tetris_margin + self.tetris_cell_size * (self.tetris_piece_c + c),
                                             self.tetris_top + self.tetris_margin + self.tetris_cell_size * (self.tetris_piece_r + r),
                                             20 + self.tetris_margin + self.tetris_cell_size * (self.tetris_piece_c + c + 1),
                                             self.tetris_top + self.tetris_margin + self.tetris_cell_size * (self.tetris_piece_r + r + 1),
                                             fill='red', width=3)

    def draw_teddy(self):
        self.canvas.create_image(20, self.teddy_y, image=self.teddy_img, anchor=NW)

    def scroll_draw_text(self):
        for i in range(len(self.text)):
            if -1000 <= self.text_top[i] < self.height:
                self.canvas.create_text(10, self.text_top[i], text=self.text[i], anchor=NW, fill='white', font=('Courier', 14))
        # self.canvas.create_text(10, self.text_top, text=self.text, anchor=NW, fill='white', font=('Courier New', 14))

    def scroll_draw_bg(self):
        self.canvas.create_rectangle(-10, -10, self.width + 10, self.height + 10, fill='black')

    def splash_redraw_all(self):
        if self.transition_to_scroll:
            self.splash_draw_bg()
        self.draw_btn()

    def splash_draw_bg(self):
        self.canvas.create_rectangle(-10, -10,
                                     self.width + 10, self.height + 10,
                                     fill=rgb(*self.splash_transition_fill))

    def draw_btn(self):
        self.draw_btn_bot()
        self.draw_btn_cyl()
        self.draw_btn_top()
        self.draw_btn_triag()

    def draw_btn_bot(self):
        self.canvas.create_oval(self.width / 2 - self.btn_major,
                                self.height / 2 - self.btn_minor,
                                self.width / 2 + self.btn_major,
                                self.height / 2 + self.btn_minor,
                                outline='', fill=rgb(*self.btn_shade_fill))

    def draw_btn_cyl(self):
        self.canvas.create_rectangle(self.width / 2 - self.btn_major,
                                     self.height / 2 - self.btn_height,
                                     self.width / 2 + self.btn_major,
                                     self.height / 2,
                                     outline='', fill=rgb(*self.btn_shade_fill))

    def draw_btn_top(self):
        self.canvas.create_oval(self.width / 2 - self.btn_major,
                                self.height / 2 - self.btn_height - self.btn_minor,
                                self.width / 2 + self.btn_major,
                                self.height / 2  - self.btn_height + self.btn_minor,
                                outline='', fill=rgb(*self.btn_top_fill))

    def draw_btn_triag(self):
        h = self.btn_major
        b = self.btn_minor
        x0 = self.width / 2 - h * 2 / 5
        cy = self.height / 2 - self.btn_height
        pts = [(x0, cy - b / 2),
               (x0, cy + b / 2),
               (x0 + h, cy)]
        self.canvas.create_polygon(pts, outline='', fill=rgb(*self.btn_shade_fill))

    def run(self):
        self.timer_fired()
        self.root.mainloop()

AutoSim(720, 540).run()
